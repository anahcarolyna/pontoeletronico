<h1>Ponto Eletronico</h1> 

<p align="center">
  <img src="http://img.shields.io/static/v1?label=TESTES&message=%3E100&color=GREEN&style=for-the-badge"/>
   <img src="http://img.shields.io/static/v1?label=STATUS&message=CONCLUIDO&color=GREEN&style=for-the-badge"/>
</p>

> Status do Projeto: :heavy_check_mark:  (concluido conforme requisitos iniciais)

### Tópicos 

:small_blue_diamond: [Descrição do projeto](#descrição-do-projeto)

:small_blue_diamond: [Funcionalidades](#funcionalidades)

:small_blue_diamond: [Deploy da Aplicação](#deploy-da-aplicação-dash)

:small_blue_diamond: [Pré-requisitos](#pré-requisitos)


... 

Insira os tópicos do README em links para facilitar a navegação do leitor

## Descrição do projeto 

<p align="justify">
 Criação de um sistema simples para controle de ponto de uma
empresa. O sistema deve permitir o cadastro de usuários e o registro de ponto dos mesmos.
</p>

## Funcionalidades

:heavy_check_mark: Criação de usuário  

:heavy_check_mark: Edição de usuário  

:heavy_check_mark: Consulta de usuário por código do usuário  

:heavy_check_mark: Listar todos os usuários do sistema  

## Deploy da Aplicação :dash:

> Link do GITHUB da aplicação. https://gitlab.com/anahcarolyna/pontoeletronico



## Como rodar a aplicação :arrow_forward:

No terminal, clone o projeto: 

```
https://gitlab.com/anahcarolyna/pontoeletronico.git
```


## Como rodar os testes

Abaixo, segue todas os exemplos de como efetuar o teste.

## Casos de Uso


## JSON :floppy_disk:

### Usuários: 

ENDPOINT: /usuarios
<br></br>

Caso 1: Criação: todos os atributos devem ser preenchidos, com exceção do id, que será gerado
automaticamente no momento do cadastro.

- a tag dataCadastro é opcional na inclusão;
<br></br>

POST /usuarios - Exemplo de Body <br></br>
{<br></br>

    "nomeCompleto": "Vinicius",
    "email": "vinicius@gmail.com",
    "cpf": "10649257073",
    "dataCadastro" :"2020-07-05",
     "senha":"123456"
}
</p>
<br></br>

Exemplo de Retorno:<br></br>
{<br></br>

    "id": 6,
    "nomeCompleto": "Vinicius",
    "cpf": "10649257073",
    "email": "alegria@gmail.com",
    "dataCadastro": "2020-07-05",
    "senha": "$2a$10$m.B6VOPNLoHJDf/JgBJy3enS8qargLikComCpSGxn31YLg.lz51r."
}<br></br>
</p>
<br></br>
Caso 2: Edição: todos os campos são editáveis, com exceção do id e da data de cadastro.
<br></br>

PUT /usuarios/{id} - Exemplo de Body<br></br>
{<br></br>

    "nomeCompleto": "Mariana Vasques",
    "email": "mariaSuele@gmail.com",
    "cpf": "02567712860"
}<br></br>
</p>
<br></br>
Exemplo de Retorno:<br></br>
{<br></br>

    "id": 1,
    "nomeCompleto": "Mariana Vasques",
    "cpf": "02567712860",
    "email": "mariaSuele@gmail.com",
    "dataCadastro": "2020-05-07"
}<br></br>


Caso 3: Consulta: deve-se exibir os dados de um usuário de acordo com id informado.


GET /usuarios/{id}


Exemplo de Retorno:
{

    "id": 3,
    "nomeCompleto": "Mariana",
    "cpf": "02567712860",
    "email": "maria@gmail.com",
    "dataCadastro": "2020-07-05"
}


Caso 4: Listagem: deve ser feita a listagem de todos os usuários cadastrados na base.

GET /usuarios

Exemplo de Retorno:

[

    {
        "id": 1,
        "nomeCompleto": "Mariana Vasques",
        "cpf": "02567712860",
        "email": "mariaSuele@gmail.com",
        "dataCadastro": "2020-05-07"
    },
    {
        "id": 3,
        "nomeCompleto": "Mariana",
        "cpf": "02567712860",
        "email": "maria@gmail.com",
        "dataCadastro": "2020-07-05"
    },
    {
        "id": 4,
        "nomeCompleto": "Cristina",
        "cpf": "32923533860",
        "email": "carlos@gmail.com",
        "dataCadastro": "2020-07-05"
    }
]

Caso 5: Exclusão de usuários

DELETE /usuarios/{id} - Exemplo de Body
Retorno - HTTP Status 204

Caso 6: Realizar o login
POST - /login
{

    "cpf": "34627263821",
    "senha": "123456"
}
Retorno HTTP Status: 200

### Ponto Eletronico: 

ENDPOINT: /pontoeletronico

Caso 1: Criação: cadastro uma batida de ponto (seja entrada ou saída) para um usuário específico, de
acordo com o id informado.

Regras consideradas: 
 - horário não pode ser posterior a data/hora atual;
 - Não pode ser efetuado uma saída se não houver uma entrada;
 - Não é possível efetuar uma entrada seguida de outra entrada e nem uma saída seguida de outra saída.

POST /pontoeletronico - Exemplo de Body 
{

    "tipoBatida" : "ENTRADA",
    "usuario" : {"id": "3"},
    "dataHoraBatida" : "2020-07-05 20:15:00"
}

Retorno:
{

    "id": 10,
    "dataHoraBatida": "2020-07-05 20:15:00",
    "tipoBatida": "ENTRADA",
    "usuario": {
        "id": 3,
        "nomeCompleto": "Mariana",
        "cpf": "02567712860",
        "email": "maria@gmail.com",
        "dataCadastro": "2020-07-05"
    }
}


Caso 2: Listagem: listagem de todas as batidas de ponto de um único usuário, de acordo com o id
informado. Deve-se mostrar na resposta, além da lista de batidas, o total de horas trabalhadas
por esse usuário.

GET /pontoeletronico/{id}

- O total de horas é considerando em horas e nem em minutos.
Retorno:

{

    "batidasPonto": [
        {
            "id": 8,
            "dataHoraBatida": "2020-07-05 18:00:00",
            "tipoBatida": "ENTRADA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Mariana",
                "cpf": "02567712860",
                "email": "maria@gmail.com",
                "dataCadastro": "2020-07-05"
            }
        },
        {
            "id": 9,
            "dataHoraBatida": "2020-07-05 18:15:00",
            "tipoBatida": "SAIDA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Mariana",
                "cpf": "02567712860",
                "email": "maria@gmail.com",
                "dataCadastro": "2020-07-05"
            }
        }
    ],
    "totalDeHorasTrabalhada": 0.25
}



## Iniciando/Configurando banco de dados

Foi utilizado o MariaDB para utilização dessa API.

Segue configurações do banco:

CREATE DATABASE pontoEletronico;<br></br>
CREATE USER 'pontouser'@'localhost' IDENTIFIED BY '123ponto';<br></br>
GRANT ALL ON pontoEletronico.*TO'pontouser'@'%'IDENTIFIED BY '123ponto' WITH GRANT OPTION;


## Tarefas em aberto

Se for o caso, liste tarefas/funcionalidades que ainda precisam ser implementadas na sua aplicação

:memo: Implementação de algumas validações na atualização do cadastro de usuario;



## Desenvolvedores/Contribuintes :

Liste o time responsável pelo desenvolvimento do projeto

| [<img src="https://gitlab.com/uploads/-/system/user/avatar/6195573/avatar.png?width=400" width=115><br><sub>Ana Carolina Costa</sub>](https://gitlab.com/anahcarolyna) 
| :---: 


## Licença 

The Carol license

Copyright :copyright: 2020 - Ponto Eletronico