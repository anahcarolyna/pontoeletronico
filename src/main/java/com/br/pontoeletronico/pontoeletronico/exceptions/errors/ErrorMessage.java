package com.br.pontoeletronico.pontoeletronico.exceptions.errors;

import java.util.HashMap;

public class ErrorMessage {
    private String erro;
    private String mensagemDeErro;
    private HashMap<String, ErrorObject> camposDeErro;

    public ErrorMessage(String erro, String mensagemDeErro, HashMap<String, ErrorObject> camposDeErro) {
        this.erro = erro;
        this.mensagemDeErro = mensagemDeErro;
        this.camposDeErro = camposDeErro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String, ErrorObject> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ErrorObject> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}
