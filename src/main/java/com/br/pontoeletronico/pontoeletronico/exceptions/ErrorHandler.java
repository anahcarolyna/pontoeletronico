package com.br.pontoeletronico.pontoeletronico.exceptions;

import com.br.pontoeletronico.pontoeletronico.exceptions.errors.ErrorMessage;
import com.br.pontoeletronico.pontoeletronico.exceptions.errors.ErrorObject;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ErrorMessage manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        HashMap<String, ErrorObject> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        List<FieldError> fieldErrors = resultado.getFieldErrors();

        for(FieldError erro : fieldErrors){
            erros.put(erro.getField(), new ErrorObject(erro.getDefaultMessage(), erro.getRejectedValue().toString()));
        }

        ErrorMessage mensagemDeErro = new ErrorMessage(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                "Campos obrigatórios não preenchidos", erros);
        return mensagemDeErro;
    }
}
