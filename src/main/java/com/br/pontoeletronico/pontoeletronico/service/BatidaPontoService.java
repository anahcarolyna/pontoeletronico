package com.br.pontoeletronico.pontoeletronico.service;


import com.br.pontoeletronico.pontoeletronico.enums.TipoBatidaEnum;
import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;
import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.models.dtos.BatidaPontoRespostaDTO;
import com.br.pontoeletronico.pontoeletronico.repositories.BatidaPontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import java.time.format.DateTimeFormatter;

import java.util.ArrayList;

import java.util.List;

@Service
public class BatidaPontoService {

    @Autowired
    private BatidaPontoRepository batidaPontoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public BatidaPonto registrarPonto(BatidaPonto batidaPonto) {

        Usuario usuario = usuarioService.buscarPorID(batidaPonto.getUsuario().getId());
        batidaPonto.setUsuario(usuario);

        if( batidaPonto.getDataHoraBatida().isAfter(LocalDateTime.now())){
            throw new RuntimeException("Marcação incorreta. O horário deve ser menor ou igual a data/hora atual");
        }

        if (!verificarSeHouveBatidaDePonto(batidaPonto)) {
            throw new RuntimeException("Marcação incorreta. Favor verificar");
        }

        BatidaPonto batidaPontoObjeto = batidaPontoRepository.save(batidaPonto);

        return batidaPontoObjeto;
    }

    public BatidaPontoRespostaDTO listarTodasAsBatidasPonto(int id) {

        try {
            Usuario usuario = usuarioService.buscarPorID(id);
            Iterable<BatidaPonto> batidasPonto = batidaPontoRepository.findAllByUsuario(usuario);

            List<BatidaPonto> pontosMarcados = new ArrayList<>();

            LocalDateTime entrada = null;
            LocalDateTime saida = null;
            long quantidadeHoras = 0;

            if(((List)batidasPonto).size() == 0){
                throw  new RuntimeException("Não há registros de pontos para esse usuário");
            }

            for (BatidaPonto ponto : batidasPonto) {
                pontosMarcados.add(ponto);


                if (ponto.getTipoBatida() == TipoBatidaEnum.ENTRADA) {
                    entrada = ponto.getDataHoraBatida();
                } else {
                    saida = ponto.getDataHoraBatida();
                }

                if (entrada != null && saida != null) {
                    Duration duracao = Duration.between(entrada, saida);
                    quantidadeHoras += duracao.toMinutes();
                    entrada = null;
                    saida = null;
                }
            }

            BatidaPontoRespostaDTO batidaPontoRespostaDTO = new BatidaPontoRespostaDTO(pontosMarcados,
                    converterMinutosParaHoras(quantidadeHoras));

            return batidaPontoRespostaDTO;

        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    private String converterMinutosParaHoras(long tempo){
        long minutos = tempo %60;
        long horas = (tempo - minutos)/60;
        String concatenar = String.format("%02d", horas) + ":" + String.format("%02d", minutos) ;

        return concatenar;
    }

    private boolean verificarSeHouveBatidaDePonto(BatidaPonto batidaPonto) {
        boolean existeBatida = false;

        Iterable<BatidaPonto> batidasDePonto = batidaPontoRepository.findAllByUsuario(
                batidaPonto.getUsuario());

        if (((List)batidasDePonto).size() == 0 && batidaPonto.getTipoBatida().equals(TipoBatidaEnum.ENTRADA)) {
            existeBatida = true;
        } else {
            for (int contador = ((List) batidasDePonto).size() - 1; contador == ((List) batidasDePonto).size() - 1; contador--) {

                LocalDateTime ultimaBatida = ((List<BatidaPonto>) batidasDePonto).get(contador).getDataHoraBatida();

                if (ultimaBatida.isBefore(batidaPonto.getDataHoraBatida())) {

                    String dataFormatada = LocalDateTime.now().plusDays(-1).format(DateTimeFormatter.ISO_DATE);

                    if (ultimaBatida.isAfter(LocalDate.parse(dataFormatada).atTime(23,59,59))) {

                        if (((List<BatidaPonto>) batidasDePonto).get(contador).getTipoBatida().equals(TipoBatidaEnum.ENTRADA)
                                && batidaPonto.getTipoBatida().equals(TipoBatidaEnum.SAIDA)) {
                            existeBatida = true;
                        } else if (batidaPonto.getTipoBatida().equals(TipoBatidaEnum.ENTRADA) && ((List<BatidaPonto>) batidasDePonto).
                                get(contador).getTipoBatida().equals(TipoBatidaEnum.SAIDA)) {
                            existeBatida = true;
                        }
                    } else if (ultimaBatida.isBefore(LocalDate.parse(dataFormatada).atTime(23,59,59))) {

                        if (((List<BatidaPonto>) batidasDePonto).get(contador).getTipoBatida().equals(TipoBatidaEnum.ENTRADA)
                                && batidaPonto.getTipoBatida().equals(TipoBatidaEnum.SAIDA)) {
                            existeBatida = true;
                        } else if (batidaPonto.getTipoBatida().equals(TipoBatidaEnum.ENTRADA) && ((List<BatidaPonto>) batidasDePonto).
                                get(contador).getTipoBatida().equals(TipoBatidaEnum.SAIDA)) {
                            existeBatida = true;
                        }
                    }else if (batidaPonto.getTipoBatida().equals(TipoBatidaEnum.ENTRADA)) {
                        existeBatida = true;
                    }

                }
            }

        }

        return existeBatida;
    }
}
