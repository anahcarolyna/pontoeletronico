package com.br.pontoeletronico.pontoeletronico.service;

import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Iterable<Usuario> buscarTodos(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }


    public Usuario buscarPorID(int id){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if(optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }

        throw new RuntimeException("Usuário não cadastrado.");
    }

    public Usuario criarUsuario(Usuario usuario){
        validarCampos(usuario);

        String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());
        usuario.setSenha(encoder);

        if(usuario.getDataCadastro() == null){
            LocalDate data = LocalDate.now();
            usuario.setDataCadastro(data);
        }

        Usuario usuarioObjeto = usuarioRepository.save(usuario);

        return  usuarioObjeto;
    }

    private void validarCampos(Usuario usuario){
        if(usuarioRepository.existsByCpf(usuario.getCpf())){
            throw new RuntimeException("CPF já cadastrado.");
        }

        if(usuarioRepository.existsByEmail(usuario.getEmail())){
            throw new RuntimeException("Email já cadastrado.");
        }
    }

    public Usuario atualizarUsuario(int id, Usuario usuario) {
        if(usuarioRepository.existsById(id)) {

            Usuario usuarioAnterior = buscarPorID(id);
            if(usuario.getDataCadastro() != null && usuarioAnterior.getDataCadastro() != usuario.getDataCadastro()){
                throw new RuntimeException("Não é possível alterar a data de cadastro");
            }

            String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());
            usuario.setSenha(encoder);

            usuario.setId(id);
            usuario.setDataCadastro(usuarioAnterior.getDataCadastro());
            Usuario usuarioObjeto = usuarioRepository.save(usuario);

            return usuarioObjeto;
        }

        throw new RuntimeException("Usuário não cadastrado");
    }

    public void deletarUsuario(int id){
        if(usuarioRepository.existsById(id)){
            usuarioRepository.deleteById(id);
        }else{
            throw new RuntimeException("Usuário não cadastrado");
        }
    }

}
