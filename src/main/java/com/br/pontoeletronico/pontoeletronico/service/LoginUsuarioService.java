package com.br.pontoeletronico.pontoeletronico.service;

import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.repositories.UsuarioRepository;
import com.br.pontoeletronico.pontoeletronico.security.LoginUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginUsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByCpf(cpf);

        if(usuario == null){
            throw new UsernameNotFoundException(cpf);
        }
        LoginUsuario loginUsuario = new LoginUsuario(usuario.getId(), usuario.getCpf(), usuario.getSenha());
        return loginUsuario;
    }
}
