package com.br.pontoeletronico.pontoeletronico.models.dtos;

import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;

import java.math.BigDecimal;
import java.util.List;

public class BatidaPontoRespostaDTO {
    private List<BatidaPonto> batidasPonto;
    private String totalDeHorasTrabalhada;

    public BatidaPontoRespostaDTO() {
    }

    public BatidaPontoRespostaDTO(List<BatidaPonto> batidasPonto, String totalDeHorasTrabalhada) {
        this.batidasPonto = batidasPonto;
        this.totalDeHorasTrabalhada = totalDeHorasTrabalhada;
    }

    public List<BatidaPonto> getBatidasPonto() {
        return batidasPonto;
    }

    public void setBatidasPonto(List<BatidaPonto> batidasPonto) {
        this.batidasPonto = batidasPonto;
    }

    public String getTotalDeHorasTrabalhada() {
        return totalDeHorasTrabalhada;
    }

    public void setTotalDeHorasTrabalhada(String totalDeHorasTrabalhada) {
        this.totalDeHorasTrabalhada = totalDeHorasTrabalhada;
    }
}
