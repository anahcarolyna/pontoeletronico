package com.br.pontoeletronico.pontoeletronico.models.dtos;

public class CredenciaisDTO {

    private String cpf;
    private String senha;

    public CredenciaisDTO() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
