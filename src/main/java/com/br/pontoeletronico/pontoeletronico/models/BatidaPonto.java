package com.br.pontoeletronico.pontoeletronico.models;

import com.br.pontoeletronico.pontoeletronico.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class BatidaPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dataHoraBatida;

    @Enumerated(EnumType.STRING)
    private TipoBatidaEnum tipoBatida;

    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    public BatidaPonto() {
    }

    public BatidaPonto(int id, LocalDateTime dataHoraBatida, TipoBatidaEnum tipoBatidaEnum, Usuario usuario) {
        this.id = id;
        this.dataHoraBatida = dataHoraBatida;
        this.tipoBatida = tipoBatidaEnum;
        this.usuario = usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatida = tipoBatidaEnum;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
