package com.br.pontoeletronico.pontoeletronico.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnoreProperties(value = {"dataCadastro"}, allowGetters = true)
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do nome")
    private String nomeCompleto;

    @CPF(message = "CPF inválido")
    @Column(unique = true)
    @NotNull(message = "É obrigatório o preenchimento do CPF")
    private String cpf;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "Email não pode ser nulo")
    @Column(unique = true)
    private String email;

    private LocalDate dataCadastro;

    private String senha;

    public Usuario() {
    }

    public Usuario(int id, @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a  100 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do nome") String nomeCompleto, @CPF(message = "CPF inválido")
    @NotNull(message = "É obrigatório o preenchimento do CPF") String cpf, @Email(message = "O formato do email é inválido")
    @NotNull(message = "Email não pode ser nulo") String email, LocalDate dataCadastro, String senha) {
        this.id = id;
        this.nomeCompleto = nomeCompleto;
        this.cpf = cpf;
        this.email = email;
        this.dataCadastro = dataCadastro;
        this.senha = senha;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
