package com.br.pontoeletronico.pontoeletronico.configurations;

import com.br.pontoeletronico.pontoeletronico.security.FiltroAutorizacao;
import com.br.pontoeletronico.pontoeletronico.security.FiltroDeAutenticacao;
import com.br.pontoeletronico.pontoeletronico.security.JWTUtil;
import com.br.pontoeletronico.pontoeletronico.service.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;


    private static final String[] PUBLIC_MATCHERS_GET = {
            "/usuarios",
            "/usuarios/*",
            "/pontoeletronico/*",
            "/pontoeletronico"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/usuarios",
            "/pontoeletronico",
            "/login"
    };

    private static final String[] PUBLIC_MATCHERS_PUT = {
            "/usuarios/*",
    };

    private static final String[] PUBLIC_MATCHERS_DELETE = {
            "/usuarios"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors();

        http.authorizeRequests().antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .antMatchers(HttpMethod.PUT, PUBLIC_MATCHERS_PUT).permitAll()
                .antMatchers(HttpMethod.DELETE,PUBLIC_MATCHERS_DELETE).permitAll()
                .anyRequest().authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Filtros JWT
        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
    }

    @Bean
    CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    @Bean
    BCryptPasswordEncoder bBCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bBCryptPasswordEncoder());
    }
}
