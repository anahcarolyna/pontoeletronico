package com.br.pontoeletronico.pontoeletronico.repositories;

import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;
import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface BatidaPontoRepository extends CrudRepository<BatidaPonto, Integer> {

    Iterable<BatidaPonto> findAllByUsuario(Usuario usuario);
}
