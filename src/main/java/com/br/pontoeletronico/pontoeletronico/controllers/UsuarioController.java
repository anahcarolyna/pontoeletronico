package com.br.pontoeletronico.pontoeletronico.controllers;

import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody @Valid Usuario usuario){
       try {
           return usuarioService.criarUsuario(usuario);
       }catch (RuntimeException exception){
           throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
       }
    }

    @GetMapping
    public Iterable<Usuario> exibirTodos(){

        Iterable<Usuario> usuarios = usuarioService.buscarTodos();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Usuario buscarPorID(@PathVariable int id){
        try {
            Usuario usuario = usuarioService.buscarPorID(id);
            return usuario;
        }
        catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Usuario atualizarUsuario(@PathVariable(name = "id") int id, @RequestBody Usuario usuario){
        try {
            Usuario usuarioObjeto = usuarioService.atualizarUsuario(id, usuario);
            return usuarioObjeto;
        }
        catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deletarUsuario(@PathVariable int id){
        try{
            usuarioService.deletarUsuario(id);
            return ResponseEntity.status(204).body("");
        }
        catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
