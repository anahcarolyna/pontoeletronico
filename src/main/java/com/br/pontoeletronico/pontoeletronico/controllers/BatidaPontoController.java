package com.br.pontoeletronico.pontoeletronico.controllers;

import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;
import com.br.pontoeletronico.pontoeletronico.models.dtos.BatidaPontoRespostaDTO;
import com.br.pontoeletronico.pontoeletronico.service.BatidaPontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/pontoeletronico")
public class BatidaPontoController {

    @Autowired
    private BatidaPontoService batidaPontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaPonto registrarPonto(@RequestBody @Valid BatidaPonto batidaPonto){
        try {
            return batidaPontoService.registrarPonto(batidaPonto);
        }catch (RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public BatidaPontoRespostaDTO buscarPorID(@PathVariable int id){
        try {
            BatidaPontoRespostaDTO batidaPontoRespostaDTO = batidaPontoService.listarTodasAsBatidasPonto(id);
            return batidaPontoRespostaDTO;
        }
        catch(RuntimeException exception){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
