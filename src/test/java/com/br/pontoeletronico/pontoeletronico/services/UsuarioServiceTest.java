package com.br.pontoeletronico.pontoeletronico.services;

import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.repositories.UsuarioRepository;
import com.br.pontoeletronico.pontoeletronico.service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("34627263821");
        usuario.setEmail("anahcarolyna@gmail.com");
        usuario.setNomeCompleto("Ana Carolina Costa");
        usuario.setSenha("123456");
    }

    @Test
    public void testarBuscarPorTodosOsUsuarios(){
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuarioIterable = usuarioService.buscarTodos();

        Assertions.assertEquals(usuarios, usuarioIterable);
    }

    @Test
    public void testarBuscarPorID(){
        Optional<Usuario> optionalUsuario =  Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        Usuario usuarioObjeto = usuarioService.buscarPorID(Mockito.anyInt());

        Assertions.assertEquals(usuarioObjeto, optionalUsuario.get());

    }

    @Test
    public void testarBuscarPorIDNaoEncontrado(){
        Optional<Usuario> optionalUsuario =  Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarPorID(Mockito.anyInt());});
    }

    @Test
    public void testarAtualizarUsuarioNaoEncontrado(){
        boolean existeUsuario = false;
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(existeUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.atualizarUsuario(Mockito.anyInt(), usuario);});
    }

    @Test
    public void testarAtualizarUsuarioComSucesso(){
        boolean existeUsuario = true;
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(existeUsuario);

        Optional<Usuario> optionalUsuario =  Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        usuario.setId(1);

        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.atualizarUsuario(1, usuario);

        Assertions.assertSame(usuario, usuarioObjeto);
    }

    @Test
    public void testarDeletarUsuarioNaoEncontrado(){
        boolean existeUsuario = false;
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(existeUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.deletarUsuario(Mockito.anyInt());});
    }

    @Test
    public void testarDeletarUsuarioComSucesso(){
        boolean existeUsuario = true;
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(existeUsuario);

        usuarioService.deletarUsuario(Mockito.anyInt());
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(Mockito.anyInt());

    }
}
