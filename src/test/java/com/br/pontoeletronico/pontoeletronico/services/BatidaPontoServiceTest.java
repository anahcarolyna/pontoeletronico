package com.br.pontoeletronico.pontoeletronico.services;

import com.br.pontoeletronico.pontoeletronico.enums.TipoBatidaEnum;
import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;
import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.models.dtos.BatidaPontoRespostaDTO;
import com.br.pontoeletronico.pontoeletronico.repositories.BatidaPontoRepository;
import com.br.pontoeletronico.pontoeletronico.service.BatidaPontoService;
import com.br.pontoeletronico.pontoeletronico.service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class BatidaPontoServiceTest {

    @MockBean
    private BatidaPontoRepository batidaPontoRepository;

    @MockBean
    private UsuarioService usuarioService;


    @Autowired
    private BatidaPontoService batidaPontoService;

    Usuario usuario;
    BatidaPonto batidaPonto;
    List<BatidaPonto> listaPontos;
    BatidaPontoRespostaDTO batidaPontoRespostaDTO;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("34627263821");
        usuario.setEmail("anahcarolyna@gmail.com");
        usuario.setNomeCompleto("Ana Carolina Costa");
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());

        batidaPonto = new BatidaPonto();
        batidaPonto.setDataHoraBatida(LocalDateTime.now().minusMinutes(60));
        batidaPonto.setUsuario(usuario);
        batidaPonto.setId(1);
        batidaPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        listaPontos = new ArrayList<>();
        listaPontos.add(batidaPonto);

        batidaPontoRespostaDTO = new BatidaPontoRespostaDTO(listaPontos,
                "02:31");
    }

    @Test
    public void testarRegistrarPontoUsuarioNaoEncontrado(){
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {batidaPontoService.registrarPonto(batidaPonto);});
    }

    @Test
    public void testarRegistrarPontoComSucesso(){
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);

        BatidaPonto batidaPontoObjeto = batidaPontoService.registrarPonto(batidaPonto);

        Assertions.assertEquals(batidaPontoObjeto, batidaPontoObjeto);
    }

    @Test
    public void testarRegistrarPontoComHoraMaiorQueAtual(){
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);

        batidaPonto.setDataHoraBatida(LocalDateTime.now().plusMinutes(5));

        Assertions.assertThrows(RuntimeException.class, () -> {batidaPontoService.registrarPonto(batidaPonto);});
    }

    @Test
    public void testarRegistrarPontoComSaidaSemEntrada(){
        batidaPonto.setTipoBatida(TipoBatidaEnum.SAIDA);

        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(batidaPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(listaPontos);

        BatidaPonto batidaPontoObjeto = new BatidaPonto();
        batidaPontoObjeto.setDataHoraBatida(LocalDateTime.now());
        batidaPontoObjeto.setUsuario(usuario);
        batidaPontoObjeto.setId(1);
        batidaPontoObjeto.setTipoBatida(TipoBatidaEnum.SAIDA);

        Assertions.assertThrows(RuntimeException.class, () -> {batidaPontoService.registrarPonto(batidaPontoObjeto);});
    }

    @Test
    public void testarRegistrarPontoComDataMenorQuePontoAnterior(){
        batidaPonto.setDataHoraBatida(LocalDateTime.now().minusMinutes(5));

        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(batidaPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(listaPontos);

        BatidaPonto batidaPontoObjeto = new BatidaPonto();
        batidaPontoObjeto.setDataHoraBatida(LocalDateTime.now().minusMinutes(10));
        batidaPontoObjeto.setUsuario(usuario);
        batidaPontoObjeto.setId(1);
        batidaPontoObjeto.setTipoBatida(TipoBatidaEnum.SAIDA);

        Assertions.assertThrows(RuntimeException.class, () -> {batidaPontoService.registrarPonto(batidaPontoObjeto);});
    }

    @Test
    public void testarListarTodosPontosBatidoPorUsuarioComSucesso(){
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);

        Iterable<BatidaPonto> batidaPontos = Arrays.asList(batidaPonto);

        Mockito.when(batidaPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(batidaPontos);

        BatidaPontoRespostaDTO batidaPontoRespostaDTOObjeto = batidaPontoService.listarTodasAsBatidasPonto(Mockito.anyInt());

        Assertions.assertEquals(((List)batidaPontos).size(),
                batidaPontoRespostaDTO.getBatidasPonto().size());
    }

    @Test
    public void testarListarTodosPontosBatidoPorUsuarioSemDados(){
        batidaPonto = new BatidaPonto();

        Mockito.when(batidaPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {batidaPontoService.listarTodasAsBatidasPonto(Mockito.anyInt());});

    }
}
