package com.br.pontoeletronico.pontoeletronico.controllers;

import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.security.JWTUtil;
import com.br.pontoeletronico.pontoeletronico.service.LoginUsuarioService;
import com.br.pontoeletronico.pontoeletronico.service.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("34627263821");
        usuario.setEmail("anahcarolyna@gmail.com");
        usuario.setNomeCompleto("Ana Carolina Costa");
    }

    @Test
    public void testarExibirTodosOsUsuarios() throws Exception {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioService.buscarTodos()).thenReturn(usuarios);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscarPorIdComSucesso() throws Exception {
        usuario.setId(1);
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenReturn(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado() throws Exception {
        Mockito.when(usuarioService.buscarPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/50")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCriarUsuarioValido() throws Exception {
        Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuario.setId(1);
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo((LocalDate.now().toString()))));
    }

    @Test
    public void testarAtualizarUsuarioNaoEncontrado() throws Exception {
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyInt(),Mockito.any(Usuario.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario= mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/50")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarUsuarioComSucesso() throws Exception {
        usuario.setId(1);
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyInt(), Mockito.any(Usuario.class)))
                .thenReturn(usuario);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }


    @Test
    @WithMockUser
    public void testarDeletarUsuarioComSucesso() throws Exception{
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/usuarios/{id}",1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(usuarioService, Mockito.times(1)).deletarUsuario(Mockito.anyInt());
    }

    @Test
    @WithMockUser
    public void testarDeletarUsuarioNaoEncontrado() throws Exception {
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario= mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/50")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
