package com.br.pontoeletronico.pontoeletronico.controllers;

import com.br.pontoeletronico.pontoeletronico.enums.TipoBatidaEnum;
import com.br.pontoeletronico.pontoeletronico.models.BatidaPonto;
import com.br.pontoeletronico.pontoeletronico.models.Usuario;
import com.br.pontoeletronico.pontoeletronico.models.dtos.BatidaPontoRespostaDTO;
import com.br.pontoeletronico.pontoeletronico.security.JWTUtil;
import com.br.pontoeletronico.pontoeletronico.service.BatidaPontoService;
import com.br.pontoeletronico.pontoeletronico.service.LoginUsuarioService;
import com.br.pontoeletronico.pontoeletronico.service.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(BatidaPontoController.class)
public class BatidaPontoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BatidaPontoService batidaPontoService;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    Usuario usuario;
    BatidaPonto batidaPonto;
    List<BatidaPonto> listaPontos;
    BatidaPontoRespostaDTO batidaPontoRespostaDTO;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("34627263821");
        usuario.setEmail("anahcarolyna@gmail.com");
        usuario.setNomeCompleto("Ana Carolina Costa");
        usuario.setId(1);

        batidaPonto = new BatidaPonto();
        batidaPonto.setUsuario(usuario);
        batidaPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        listaPontos = new ArrayList<>();
        listaPontos.add(batidaPonto);

       batidaPontoRespostaDTO = new BatidaPontoRespostaDTO(listaPontos,
               "02:21");
    }

    @Test
    public void testarRegistrarPedidoSemSucesso() throws Exception {

        Mockito.when(batidaPontoService.registrarPonto(Mockito.any(BatidaPonto.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(batidaPonto);

        mockMvc.perform(MockMvcRequestBuilders.post("/pontoeletronico")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarRegistrarPontoComSucesso() throws Exception {

        Mockito.when(batidaPontoService.registrarPonto(Mockito.any(BatidaPonto.class))).then(pontoObjeto -> {
            batidaPonto.setId(1);
            batidaPonto.setDataHoraBatida(LocalDateTime.now());
            return batidaPonto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(batidaPonto);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/pontoeletronico")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarPorUsuario() throws Exception {
        usuario.setId(1);
        Mockito.when(batidaPontoService.listarTodasAsBatidasPonto(Mockito.anyInt())).thenReturn(batidaPontoRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/pontoeletronico/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    public void testarBuscarPorUsuarioNaoEncontrado() throws Exception {
        Mockito.when(batidaPontoService.listarTodasAsBatidasPonto(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/pontoeletronico/20")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
